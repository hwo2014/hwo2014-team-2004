﻿using System;
using HWO2014_TheLambdaOperators.Models;

namespace HWO2014_TheLambdaOperators
{
  interface IBot
  {
    string Color { get; set; }
    //string Key { get; set; }
    string Name { get; set; }

    SendMessageBase ProcessMessage(ReceiveMessageBase msg);
  }
}
