﻿using System.Collections.Generic;

namespace HWO2014_TheLambdaOperators.Models
{
  internal class CarPositions : GameBase
  {
    public List<CarPosition> Data { get; set; }
  }

  internal class CarPosition
  {
    public BotId Id { get; set; }
    public float Angle { get; set; }
    public CarPiecePosition PiecePosition { get; set; }
  }

  internal class CarPiecePosition
  {
    public int PieceIndex { get; set; }
    public double InPieceDistance { get; set; }
    public CarLane Lane { get; set; }
    public int Lap { get; set; }
  }

  internal class CarLane
  {
    public int StartLaneIndex { get; set; }
    public int EndLaneIndex { get; set; }
  }

  internal class CarInfo
  {
    public BotId Id { get; set; }
    public CarDimensions Dimensions { get; set; }
  }

  internal class CarDimensions
  {
    public double Length { get; set; }
    public double Width { get; set; }
    public double GuideFlagPosition { get; set; }
  }

  internal class YourCar : ReceiveMessageBase
  {
    public BotId Data { get; set; }
  }
}
