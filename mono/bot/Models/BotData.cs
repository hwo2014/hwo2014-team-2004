﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class BotData
  {
    public string Name { get; set; }
    public string Color { get; set; }
    public int PieceIndex { get; set; }
    public double InPieceDistance { get; set; }
    public int Lap { get; set; }
    public int StartLaneIndex { get; set; }
    public int EndLaneIndex { get; set; }

    public TrackPiece Piece { get; set; }

    public double Acceleration { get; set; }
    public double Velocity { get; set; }
    public double Angle { get; set; }

    public double SectionLengthRemaining { get { return Piece.Section.SectionLengthRemaining(Piece, InPieceDistance, EndLaneIndex); } }

    public BotData(CarPosition cp)
    {
      Angle = cp.Angle;
      Color = cp.Id.Color;
      EndLaneIndex = cp.PiecePosition.Lane.EndLaneIndex;
      InPieceDistance = cp.PiecePosition.InPieceDistance;
      Lap = cp.PiecePosition.Lap;
      Name = cp.Id.Name;
      PieceIndex = cp.PiecePosition.PieceIndex;
      StartLaneIndex = cp.PiecePosition.Lane.StartLaneIndex;
    }
  }
}
