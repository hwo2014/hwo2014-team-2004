﻿using System.Collections.Generic;
using System.Linq;
using HWO2014_TheLambdaOperators.Utils;
using System;

namespace HWO2014_TheLambdaOperators.Models
{
  internal enum TrackType
  {
    StraightAway = 1,
    TurnLeft = 2,
    TurnRight = 3
  }

  internal class Track
  {
    private bool _analyzed = false;
    private TrackSection _currentSection = null;
    public List<TrackSection> TrackSections { get; set; }
    public string Id { get; set; }
    public string Name { get; set; }
    public int TrackDirection { get; set; }
    public int InsideLaneIndex { get; set; }

    public List<TrackLane> Lanes { get; set; }
    public TrackStartingPoint StartingPoint { get; set; }

    /// Temp property for Json deserialization
    public List<TrackPiece> Pieces { get; set; }

    public Track()
    {
      TrackSections = new List<TrackSection>();
    }

    public void Analyze()
    {
      if (_analyzed)
        return;

      double overallAngle = 0;
      Lanes = Lanes.OrderBy(l => l.Index).ToList();
      for (int i = 0; i < Pieces.Count; i++)
      {
        var piece = Pieces[i];
        piece.PieceIndexTrack = i;
        overallAngle += piece.Angle;
        piece.NextPiece = Pieces[i + 1 < Pieces.Count ? i + 1 : 0];
        piece.NextPiece.PrevPiece = piece;
        AddPiece(piece);
      }
      _currentSection.NextSection = TrackSections[0];
      _currentSection = null;

      if (overallAngle > 0)
      {
        TrackDirection = 1;
        InsideLaneIndex = Lanes.Count - 1;
      }
      else if (overallAngle < 0)
      {
        TrackDirection = -1;
        InsideLaneIndex = 0;
      }

      _analyzed = true;
    }

    private void AddPiece(TrackPiece piece)
    {
      /// Analyze piece
      var trackType = TrackType.StraightAway;
      if (piece.Angle > 0)
        trackType = TrackType.TurnRight;
      else if (piece.Angle < 0)
        trackType = TrackType.TurnLeft;

      /// If piece type or angle is different from previous piece create new track section
      /// else add it to the current section
      if (_currentSection == null)
      {
        _currentSection = new TrackSection(trackType, this);
        TrackSections.Add(_currentSection);
      }
      else if (_currentSection.SectionType != trackType ||
        _currentSection.Pieces[_currentSection.Pieces.Count - 1].Radius != piece.Radius)  // it's only the radius that matters for differentiating same direction turns
      {
        _currentSection.NextSection = new TrackSection(trackType, this);
        _currentSection = _currentSection.NextSection;
        TrackSections.Add(_currentSection);
      }
      _currentSection.AddPiece(piece);
    }

    public TrackSection GetTrackSectionFromPieceIndex(int index)
    {
      return TrackSections.FirstOrDefault(ts => ts.Pieces.Any(p => p.PieceIndexTrack == index));
    }
  }

  internal class TrackSection
  {
    public Track Track { get; private set; }

    public TrackType SectionType { get; private set; }
    public double TotalAngle { get; private set; }
    public double CenterRadius { get; private set; }

    /// Indicies of the following lists of doubles contain the value pertaining to a lane index.
    public List<double> Apexes { get; private set; }
    public List<double> Radii { get; private set; }
    public List<double> TotalLengths { get; private set; }

    public List<TrackPiece> Pieces { get; private set; }
    public TrackSection NextSection { get; set; }

    public TrackSection(TrackType sectionType, Track track)
    {
      Pieces = new List<TrackPiece>();
      Track = track;

      SectionType = sectionType;
      Apexes = new List<double>(Track.Lanes.Count);
      Radii = new List<double>(Track.Lanes.Count);
      TotalLengths = new List<double>(Track.Lanes.Count);
      for (int i = 0; i < Track.Lanes.Count; i++)
      {
        Apexes.Add(0);
        Radii.Add(0);
        TotalLengths.Add(0);
      }
    }

    public void AddPiece(TrackPiece piece)
    {
      piece.Section = this;
      piece.PieceIndexSection = Pieces.Count;
      Pieces.Add(piece);

      if (SectionType == TrackType.StraightAway)
      {
        for (int i = 0; i < Track.Lanes.Count; i++)
        {
          TotalLengths[i] += piece.Length;
        }
      }
      else
      {
        TotalAngle += piece.Angle;
        if (CenterRadius == 0 && piece.Radius > 0)
        {
          CenterRadius = piece.Radius;
          int offsetModifier = 1;
          if (SectionType == TrackType.TurnRight)
            offsetModifier = -1;

          for (int i = 0; i < Track.Lanes.Count; i++)
          {
            Radii[i] += piece.Radius + Track.Lanes[i].DistanceFromCenter * offsetModifier;
          }
        }
        for (int i = 0; i < Track.Lanes.Count; i++)
        {
          TotalLengths[i] += piece.PieceLength(i);
        }
        for (int i = 0; i < TotalLengths.Count; i++)
        {
          Apexes[i] = TotalLengths[i] / 2;
        }
      }
    }

    public double SectionPercentageComplete(TrackPiece piece, double inPieceDistance, int laneIndex)
    {
      return (Pieces.Take(piece.PieceIndexSection).Sum(p => p.PieceLength(laneIndex)) + inPieceDistance) / TotalLengths[laneIndex];
    }

    public double SectionLengthRemaining(TrackPiece piece, double inPieceDistance, int laneIndex)
    {
      var result = TotalLengths[laneIndex] - (Pieces.Take(piece.PieceIndexSection).Sum(p => p.PieceLength(laneIndex)) + inPieceDistance);
      return result;
    }
  }

  internal class TrackPiece
  {
    // Do not rename the following 4 properties
    public double Radius { get; set; }
    public double Angle { get; set; }
    public double Length { get; set; }
    public bool Switch { get; set; }

    public int PieceIndexTrack { get; set; }
    public int PieceIndexSection { get; set; }

    public TrackSection Section { get; set; }
    public TrackPiece NextPiece { get; set; }
    public TrackPiece PrevPiece { get; set; }

    public double PieceLength(int laneIndex)
    {
      if (Angle == 0f)
        return Length;

      /// When turning right the left lanes from center are on the outside but have a negative distance from center offset.
      /// We must invert the sign of offsets for right turns.
      var offsetModifier = 1;
      if (Section.SectionType == TrackType.TurnRight)
        offsetModifier = -1;

      return MathUtil.ArcLength(Angle, Radius, Section.Track.Lanes[laneIndex].DistanceFromCenter * offsetModifier);
    }
  }

  internal class TrackLane
  {
    public double DistanceFromCenter { get; set; }
    public int Index { get; set; }
  }

  internal class TrackStartingPoint
  {
    public TrackPoint Position { get; set; }
    public double Angle { get; set; }
  }

  internal class TrackPoint
  {
    public float X { get; set; }
    public float Y { get; set; }
  }
}
