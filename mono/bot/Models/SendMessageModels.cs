using System.Collections.Generic;
using Newtonsoft.Json;

namespace HWO2014_TheLambdaOperators.Models
{
  #region Send Message Models
  public class SendMessageWrapper
  {
    public string msgType { get; private set; }
    public object data { get; private set; }

    public SendMessageWrapper(string msgType, object data)
    {
      this.msgType = msgType;
      this.data = data;
    }
  }

  public class SendMessageWrapperWithGameTick : SendMessageWrapper
  {
    public int gameTick { get; private set; }

    public SendMessageWrapperWithGameTick(string msgType, object data, int gameTick) : base(msgType, data)
    {
      this.gameTick = gameTick;
    }
  }

  public abstract class SendMessageBase
  {
    public virtual string ToJson()
    {
      return JsonConvert.SerializeObject(new SendMessageWrapper(this.MsgType(), this.MsgData()));
    }

    protected virtual object MsgData()
    {
      return this;
    }

    protected abstract string MsgType();
  }

  public class Ping : SendMessageBase
  {
    protected override string MsgType()
    {
      return "ping";
    }
  }

  public class CreateRace : SendMessageBase
  {
    //public Dictionary<string, string> botId { get; private set; }
    public object botId;
    public string trackName { get; private set; }
    public string password { get; private set; }
    public int carCount { get; private set; }

    public CreateRace(string name, string key, string trackName, string password, int carCount)
    {
      botId = new
      {
        name = name,
        key = key
      };
      this.trackName = trackName;
      this.password = password;
      this.carCount = carCount;
    }

    protected override string MsgType()
    {
      return "createRace";
    }
  }

  public class Join : SendMessageBase
  {
    public string name { get; private set; }
    public string key { get; private set; }
    //public string color { get; private set; }

    public Join(string name, string key)
    {
      this.name = name;
      this.key = key;
    }

    //public Join(string name, string key, string color)
    //  : this(name, key)
    //{
    //  this.color = color.ToLower();
    //}

    protected override string MsgType()
    {
      return "join";
    }
  }

  public class SwitchLane : SendMessageBase
  {
    private string _leftOrRight;

    public SwitchLane(string leftOrRight)
    {
      _leftOrRight = leftOrRight;
    }

    protected override object MsgData()
    {
      return _leftOrRight;
    }

    protected override string MsgType()
    {
      return "switchLane";
    }
  }

  public class Throttle : SendMessageBase
  {
    private double _throttle;
    private int? _gameTick;

    public Throttle(double throttle)
    {
      _throttle = throttle;
    }

    public Throttle(double throttle, int gameTick) : this(throttle)
    {
      _gameTick = gameTick;
    }

    public override string ToJson()
    {
      if (_gameTick.HasValue)
        return JsonConvert.SerializeObject(new SendMessageWrapperWithGameTick(this.MsgType(), this.MsgData(), _gameTick.Value));
      return base.ToJson();
    }

    protected override object MsgData()
    {
      return _throttle;
    }

    protected override string MsgType()
    {
      return "throttle";
    }
  }

  public class Turbo : SendMessageBase
  {
    private string _flavourText;

    public Turbo(string flavourText)
    {
      _flavourText = flavourText;
    }

    protected override object MsgData()
    {
      return _flavourText;
    }

    protected override string MsgType()
    {
      return "turbo";
    }
  }
  #endregion
}