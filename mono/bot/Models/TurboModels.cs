﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class TurboAvailableMessage : GameBase
  {
    public TurboAvailable Data { get; set; }
  }

  internal class TurboAvailable
  {
    public double TurboDurationMilliseconds { get; set; }
    public int TurboDurationTicks { get; set; }
    public double TurboFactor { get; set; }
  }

  internal class TurboStart : GameBase
  {
    public BotId Data { get; set; }
  }

  internal class TurboEnd : GameBase
  {
    public BotId Data { get; set; }
  }
}
