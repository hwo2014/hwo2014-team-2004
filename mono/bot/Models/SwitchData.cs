﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class SwitchData
  {
    public string SwitchDirection { get; set; }
    public bool SwitchPending { get; set; }
    public bool QueueSwitch { get; set; }

    public void Reset()
    {
      SwitchDirection = null;
      SwitchPending = false;
      QueueSwitch = false;
    }
  }
}
