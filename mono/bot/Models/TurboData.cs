﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class TurboData
  {
    public bool HasTurbo { get; set; }
    //public bool TurboSent { get; set; }
    public bool QueueTurbo { get; set; }
    public bool TurboActive { get; set; }
    public bool TurboPending { get; set; }
    public double TurboDurationMilliseconds { get; set; }
    public int TurboDurationTicks { get; set; }
    public double TurboFactor { get; set; } // Car top velocity is multiplied by this factor.

    public void SetTurbo(TurboAvailable ta)
    {
      Reset(); // should we reset? Can we have multiple turbos banked? I would think not.
      HasTurbo = true;
      TurboDurationMilliseconds = ta.TurboDurationMilliseconds;
      TurboDurationTicks = ta.TurboDurationTicks;
      TurboFactor = ta.TurboFactor;
    }

    public void Reset()
    {
      HasTurbo = false;
      //TurboSent = false;
      QueueTurbo = false;
      TurboActive = false;
      TurboPending = false;
      TurboDurationMilliseconds = 0.0;
      TurboDurationTicks = 0;
      TurboFactor = 0.0;
    }
  }
}
