﻿using System.Collections.Generic;

namespace HWO2014_TheLambdaOperators.Models
{
  internal class RaceData
  {
    public Track Track { get; set; }
    public List<CarInfo> Cars { get; set; }
    public RaceSession RaceSession { get; set; }
  }

  internal class RaceSession
  {
    /// This object is used for both the Qualifier and the actual Race.
    /// In the qualifier Laps == 0, MaxLapTimeMs == 0, and DurationMs > 0.
    /// In the race Laps > 0, MaxLapTimeMs > 0, and DurationMs == 0
    /// Quick Race will be false for both. It is used on the test servers to indicate a 3 lap race.

    public int Laps { get; set; }
    public int DurationMs { get; set; }
    public int MaxLapTimeMs { get; set; }
    public bool QuickRace { get; set; }
  }
}
