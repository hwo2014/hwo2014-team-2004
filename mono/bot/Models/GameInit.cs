﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class GameInit : GameBase
  {
    public GameInitData Data { get; set; }
  }

  internal class GameInitData
  {
    public RaceData Race { get; set; }
  }
}
