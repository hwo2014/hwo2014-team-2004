﻿
namespace HWO2014_TheLambdaOperators.Models
{
  internal class ReceiveMessageBase
  {
    public string MsgType { get; set; }
  }

  internal class GameBase : ReceiveMessageBase
  {
    public string GameId { get; set; }
    public int GameTick { get; set; }

    public GameBase()
    {
      GameTick = -1;
    }
  }
}
