﻿using System.Collections.Generic;

namespace HWO2014_TheLambdaOperators.Models
{
  #region LapTime/RaceTime Shared Models
  internal abstract class CarTime
  {
    public int Ticks { get; set; }
    public int Millis { get; set; }
  }

  internal class LapTime : CarTime
  {
    public int Lap { get; set; }
  }

  internal class RaceTime : CarTime
  {
    public int Laps { get; set; }
  } 
  #endregion

  #region Lap Finished Models
  internal class LapFinishedWrapper : GameBase
  {
    public LapFinished Data { get; set; }
  }

  internal class LapFinished
  {
    public BotId Car { get; set; }
    public LapTime LapTime { get; set; }
    public RaceTime RaceTime { get; set; }
    public Ranking Ranking { get; set; }
  }

  internal class Ranking
  {
    public int Overall { get; set; }
    public int FastestLap { get; set; }
  }
  #endregion

  #region Bot Finish/DNF Models
  internal class BotFinish : GameBase
  {
    public BotId Data { get; set; }
  }

  internal class BotDnfWrapper : GameBase
  {
    public BotDnf Data { get; set; }
  }

  internal class BotDnf
  {
    public BotId Car { get; set; }
    public string Reason { get; set; }
  } 
  #endregion

  #region GameEnd (Race finished) Models
  internal class GameEndWrapper : ReceiveMessageBase
  {
    public GameEnd Data { get; set; }
  }

  internal class GameEnd
  {
    public List<RaceStandings> Results { get; set; }
    public List<BestLap> BestLaps { get; set; }
  }

  internal class RaceStandings
  {
    public BotId Car { get; set; }
    public RaceTime Result { get; set; }
  }

  internal class BestLap
  {
    public BotId Car { get; set; }
    public LapTime Result { get; set; }
  }
  #endregion
}
