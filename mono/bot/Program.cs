﻿using System;
using System.IO;
using System.Net.Sockets;
using HWO2014_TheLambdaOperators.Models;
using HWO2014_TheLambdaOperators.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace HWO2014_TheLambdaOperators
{
  public static class Program
  {
    private static Regex _regex = null;

    #region Main
    static void Main(string[] args)
    {
      string host = args[0];
      int port = int.Parse(args[1]);
      string botName = args[2];
      string botKey = args[3];

      //#if DEBUG
      //      host = "testserver.helloworldopen.com";
      //      port = 8091;
      //      botName = "LambdaPanda";
      //      botKey = "z/hwrCfYDAWZMw";
      //#endif

      Initialize();

      Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

      using (var client = new TcpClient(host, port))
      {
        var stream = client.GetStream();
        var reader = new StreamReader(stream);
        var writer = new StreamWriter(stream);
        writer.AutoFlush = true;

        Action<SendMessageBase> send = m =>
        {
          var json = m.ToJson();
          Logger.WriteLine(string.Concat(json, "\n"));
          if (writer.BaseStream.CanWrite)
            writer.WriteLine(json);
        };

        var joinMsg = new Join(botName, botKey);
        //var joinMsg = new CreateRace(botName, botKey, "suzuka", "asdf", 1);
        IBot bot = RaceInitializer(reader, writer, send, joinMsg);
        if (bot != null)
          bot.ProcessMessages(reader, writer, send);
        else
          Logger.WriteLine("Bot is null");
      }
    }

    public static void Initialize()
    {
      Logger.Initialize(new MemoryStream());
      //Logger.Initialize(new FileStream(string.Format("./Logs/Log_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd-HH-mm")), FileMode.OpenOrCreate, FileAccess.Write));

      _regex = new Regex(@"(?<=msgType"":"")(\w+\b)");

      Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);
      AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
      //AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);
    }

    //static void CurrentDomain_DomainUnload(object sender, EventArgs e)
    //{
    //  CleanUp();
    //}

    // Executes when application is finished
    static void CurrentDomain_ProcessExit(object sender, EventArgs e)
    {
      CleanUp();
    }

    /// Executes on CTRL-C
    static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
    {
      CleanUp();
    }

    public static void CleanUp()
    {
      Logger.WriteToFileThenDispose(string.Format("./Logs/Log_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")));
    }
    #endregion

    private static IBot RaceInitializer(StreamReader reader, StreamWriter writer, Action<SendMessageBase> send, SendMessageBase joinMsg)
    {
      IBot bot = null;
      YourCar carData = null;
      GameInit raceData = null;

      string line;
      send(joinMsg); // Join race
      while ((line = reader.ReadLine()) != null)
      {
        //Logger.WriteLine(JToken.Parse(line).ToString(Formatting.Indented));
        Logger.WriteLine(line);

        var receivedMsg = ParseMessage(line);
        if (receivedMsg == null)
        {
          send(new Ping());
          continue;
        }

        switch (receivedMsg.MsgType)
        {
          case"yourCar":
            Console.WriteLine("Car data received");
            carData = (YourCar)receivedMsg;
            break;
          case "gameInit":
            Console.WriteLine("Game data received");
            raceData = (GameInit)receivedMsg;
            break;
          default:
            break;
        }

        if (carData != null && raceData != null)
        {
          bot = new Bot(carData.Data, raceData.Data.Race);
          break;
        }
      }

      return bot;
    }

    private static void ProcessMessages(this IBot bot, StreamReader reader, StreamWriter writer, Action<SendMessageBase> send)
    {
      string line;
      while ((line = reader.ReadLine()) != null)
      {
        /// User regex here to determine msg type before deserializing.  Then deserialize into exact received message type.
        /// i.e. GameInit, CarPositions etc.  Classes need to be created.
        //var receivedMsg = JsonConvert.DeserializeObject<ReceiveMessageBase>(line);
        //Logger.WriteLine(string.Concat(JToken.Parse(line).ToString(Formatting.Indented), "\n"));
        Logger.WriteLine(line);

        var receivedMsg = ParseMessage(line);

        SendMessageBase responseMsg = null;
        try
        {
          responseMsg = bot.ProcessMessage(receivedMsg);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
          responseMsg = new Ping();
        }
        if (responseMsg != null)
          send(responseMsg);
      }
    }

    private static ReceiveMessageBase ParseMessage(string line)
    {
      string msgType = null;
      ReceiveMessageBase receivedMsg = null;
      var match = _regex.Match(line);
      if (match.Success)
        msgType = match.Value;

      switch (msgType)
      {
        case "carPositions":
          receivedMsg = JsonConvert.DeserializeObject<CarPositions>(line);
          break;

        case "join":
          Logger.WriteLine("Joined");
          receivedMsg = JsonConvert.DeserializeObject<Joined>(line);
          break;

        case "gameInit":
          Logger.WriteLine("Race init");
          receivedMsg = JsonConvert.DeserializeObject<GameInit>(line);
          break;
        case "gameEnd":
          Logger.WriteLine("Race Ended");
          receivedMsg = JsonConvert.DeserializeObject<GameEndWrapper>(line);
          break;
        case "gameStart":
          receivedMsg = JsonConvert.DeserializeObject<GameBase>(line);
          break;

        case "crash":
          receivedMsg = JsonConvert.DeserializeObject<CrashSpawn>(line);
          break;
        case "spawn":
          receivedMsg = JsonConvert.DeserializeObject<CrashSpawn>(line);
          break;

        case "dnf":
          receivedMsg = JsonConvert.DeserializeObject<BotDnfWrapper>(line);
          break;
        case "finish":
          receivedMsg = JsonConvert.DeserializeObject<BotFinish>(line);
          break;

        case "lapFinished":
          receivedMsg = JsonConvert.DeserializeObject<LapFinishedWrapper>(line);
          break;
        case "yourCar":
          receivedMsg = JsonConvert.DeserializeObject<YourCar>(line);
          break;

        case "turboAvailable":
          receivedMsg = JsonConvert.DeserializeObject<TurboAvailableMessage>(line);
          break;
        case "turboEnd":
          receivedMsg = JsonConvert.DeserializeObject<TurboEnd>(line);
          break;
        case "turboStart":
          receivedMsg = JsonConvert.DeserializeObject<TurboStart>(line);
          break;

        default:
          Logger.WriteLine("MESSAGE CASE NOT HANDLED: {0}", msgType);
          break;
      }
      return receivedMsg;
    }
  }
}
