using System;
using System.Linq;
using HWO2014_TheLambdaOperators.Models;
using HWO2014_TheLambdaOperators.Utils;
using System.Collections.Generic;

namespace HWO2014_TheLambdaOperators
{
  internal class BotId
  {
    public string Name { get; set; }
    public string Color { get; set; }
  }

  internal class Bot : IBot
  {
    private const string LEFT = "Left", RIGHT = "Right";

    private int _gameTick;
    private string _gameId;

    private Dictionary<double, double> _vfTurnMap = new Dictionary<double, double>()
    {
      { 25, 2.0 },
      { 30, 2.5 },
      { 35, 3.0 },
      { 40, 3.5 },
      { 45, 4.0 },
      { 50, 4.5 },
      { 55, 4.75 },
      { 60, 5.0 },
      { 65, 5.25 },
      { 70, 5.5 },
      { 75, 5.75 },
      { 80, 5.875 },
      { 85, 6.0 },
      { 90, 6.125 },
      { 95, 6.25 },
      { 100, 6.5 }, // 110, 7.228
      { 150, 7.0 },
      { 200, 7.5 },
      { 250, 8.5 },
      { 300, 9.0 }
    };

    //private double _throttle = 0.7914225;
    private double _throttle = 1.0,
      _accelCoef = 0.0,
      _drag = 0.1,
      _mass = 1.0;

    private bool _insideLaneReached = false;

    private Dictionary<string, BotData> _prevBotData, _currBotData;
    private BotData _myPrevBotData, _myCurrBotData;
    private RaceData _raceData;
    private SwitchData _switchData;
    private TurboData _turboData;

    public string Name { get; set; }
    public string Color { get; set; }
    //public string Key { get; set; }

    #region Bot Logic
    public Bot(BotId id, RaceData raceData)
    {
      _switchData = new SwitchData();
      _turboData = new TurboData();

      Name = id.Name;
      Color = id.Color;
      AnalyzeAndSetRaceData(raceData);
      Logger.WriteLine("{0} bot ({1}) Initialized.", Name, Color);
    }

    private void AnalyzeAndSetRaceData(RaceData raceData)
    {
      raceData.Track.Analyze();
      _raceData = raceData;
      Logger.WriteLine("Race Analyzed: {0}. Laps: {1}. Sections: {2}. Pieces: {3}. Lanes: {4}. Quick Race: {5}", raceData.Track.Name, raceData.RaceSession.Laps, raceData.Track.TrackSections.Count, raceData.Track.Pieces.Count, raceData.Track.Lanes.Count, raceData.RaceSession.QuickRace);
    }

    private void ResetForNewRace(RaceData raceData)
    {
      _throttle = 1.0;
      //_accelCoef = 0.0;
      //_myCurrBotData = null;
      //_myPrevBotData = null;
      //_currBotData = null;
      //_prevBotData = null;
      _turboData.Reset();
      _switchData.Reset();
      AnalyzeAndSetRaceData(raceData);
    }

    public SendMessageBase ProcessMessage(ReceiveMessageBase msg)
    {
      if (msg == null)
        return new Ping();

      SendMessageBase sendMsg = null;
      switch (msg.MsgType)
      {
        case "carPositions":
          UpdateBotData(msg as CarPositions);
          if (_prevBotData == null)
          {
            sendMsg = new Throttle(_throttle);
            break;
          }
          CalculateAdditionalBotData();
          sendMsg = DetermineAction();
          break;

        case "gameStart":
          Logger.WriteLine("Race start! Initializing Ticks and Race Id.");
          var gameBase = msg as GameBase;
          _gameId = gameBase.GameId;
          _gameTick = gameBase.GameTick;
          sendMsg = new Throttle(_throttle); // game started. Put the hammer to the floor!
          break;

        case "crash":
          var crash = msg as CrashSpawn;
          Logger.WriteLine("{0} bot ({1}) Crashed!", crash.Data.Name, crash.Data.Color);
          if (_turboData.TurboActive && Color.Equals(crash.Data.Color))
            _turboData.Reset();
          sendMsg = new Ping();
          break;
        case "spawn":
          var spawn = msg as CrashSpawn;
          Logger.WriteLine("{0} bot ({1}) spawned.", spawn.Data.Name, spawn.Data.Color);
          if (Color.Equals((msg as CrashSpawn).Data.Color))
            sendMsg = new Throttle(1.0);
          break;

        case "lapFinished":
          var lapMsg = msg as LapFinishedWrapper;
          _gameTick = lapMsg.GameTick;
          var lap = lapMsg.Data;
          Logger.WriteLine("{0} bot ({1}) Finished Lap {2} in {3} ticks ({4} ms). Race ticks {5}", lap.Car.Name, lap.Car.Color, lap.LapTime.Lap, lap.LapTime.Ticks, lap.LapTime.Millis, lap.RaceTime.Ticks);
          sendMsg = new Ping();
          break;

        case "dnf":
          var dnf = msg as BotDnfWrapper;
          Logger.WriteLine("{0} bot ({1}) is declated DNF at tick {2}. Reason: {3}.", dnf.Data.Car.Name, dnf.Data.Car.Color, dnf.GameTick, dnf.Data.Reason);
          break;
        case "finish":
          var botFinished = msg as BotFinish;
          Logger.WriteLine("{0} bot ({1}) finished race in {2} ticks.", botFinished.Data.Name, botFinished.Data.Color, botFinished.GameTick);
          break;

        case "turboAvailable":
          var turboAvailMsg = msg as TurboAvailableMessage;
          _gameTick = turboAvailMsg.GameTick;
          _turboData.SetTurbo(turboAvailMsg.Data);
          Logger.WriteLine("Turbo Online!");
          break;
        case "turboEnd":
          var turboEndMsg = msg as TurboEnd;
          _gameTick = turboEndMsg.GameTick;
          Logger.WriteLine("{0} bot ({1}) turbo ended.", turboEndMsg.Data.Name, turboEndMsg.Data.Color);
          if (Color.Equals(turboEndMsg.Data.Color)) // just in case there are other turbo messages shown to us.
            _turboData.Reset();
          sendMsg = new Ping();
          break;
        case "turboStart":
          var turboStartMsg = msg as TurboStart;
          _gameTick = turboStartMsg.GameTick;
          Logger.WriteLine("{0} bot ({1}) turbo started.", turboStartMsg.Data.Name, turboStartMsg.Data.Color);
          if (Color.Equals(turboStartMsg.Data.Color)) // just in case there are other turbo messages shown to us.
            _turboData.TurboActive = true;
          sendMsg = new Ping();
          break;

        case "yourCar":
          Logger.WriteLine("Qualifier Completed. New car data received");
          var carData = msg as YourCar;
          Color = carData.Data.Color;
          sendMsg = new Ping();
          break;
        case "gameInit":
          Logger.WriteLine("Qualifier Completed. New game data received.");
          ResetForNewRace((msg as GameInit).Data.Race);
          sendMsg = new Ping();
          break;

        case "join":
        case "gameEnd":
          //var gameEnd = msg as GameEndMessage;
        default:
          sendMsg = new Ping();
          break;
      }

      string logEntry = string.Format("Tk: {0}, T: {1:F3}, Tv: {2:F4}, V: {3:F4}, Ac: {4:F4}, A: {5:F4} Sr: {6:F4} Sc: {7:F4}, Ar {8:F4}, D {9:F4}, M {10:F4}{11}",
        _gameTick,
        _throttle,
        targetVelocity,
        _myCurrBotData.Velocity,
        _myCurrBotData.Acceleration,
        _myCurrBotData.Angle,
        _gameTick > 0 ? _myCurrBotData.SectionLengthRemaining : 0.0,
        _gameTick > 0 ? _myCurrBotData.Piece.Section.TotalLengths[_myCurrBotData.EndLaneIndex] - _myCurrBotData.SectionLengthRemaining : 0.0,
        _accelCoef,
        _drag,
        _mass,
        _turboData.TurboActive ? string.Concat(", Turbo: ", (int?)_turboData.TurboDurationTicks--) : null);
      //Console.WriteLine(logEntry); // Logger is currently writing to console as well.
      Logger.WriteLine(logEntry);
      return sendMsg;
    }

    private void UpdateBotData(CarPositions carPositions)
    {
      _gameTick = carPositions.GameTick; // will be -1 for the very first car position message since no gametick is provided and default is -1;
      _myPrevBotData = _myCurrBotData;
      _prevBotData = _currBotData;
      _currBotData = new Dictionary<string, BotData>(carPositions.Data.Count);
      foreach (var cp in carPositions.Data)
      {
        var botData = new BotData(cp);
        _currBotData[botData.Color] = botData;
        if (Color.Equals(botData.Color))
        {
          /// This is our car/bot
          _myCurrBotData = botData;
        }
      }
    }

    private void CalculateAdditionalBotData()
    {
      foreach (var kvp in _currBotData)
      {
        var currBotData = kvp.Value;
        var prevBotData = _prevBotData[currBotData.Color];
        currBotData.Piece = _raceData.Track.Pieces[currBotData.PieceIndex];

        if (prevBotData.PieceIndex != currBotData.PieceIndex)
        {
          /// we've crossed into a new piece since the last update!
          /// Here velocity is equal to the total length of the -PATH TAKEN- in the previous piece minus the distance travelled from the last update plus the distance travelled in the current piece.

          double previousPathTotalLength;
          if (prevBotData.StartLaneIndex == prevBotData.EndLaneIndex)
          {
            /// No lanes changed. PieceLength will return correct value for straightaway or curve given lane index
            previousPathTotalLength = prevBotData.Piece.PieceLength(prevBotData.EndLaneIndex);
          }
          else
          {
            /// Switched lanes in the last piece.  We must calculate the length of the diagonal to determine the length of the path taken.
            var startLane = _raceData.Track.Lanes[prevBotData.StartLaneIndex];
            var endLane = _raceData.Track.Lanes[prevBotData.EndLaneIndex];
            var horizontalDistanceTravelled = Math.Abs(startLane.DistanceFromCenter - endLane.DistanceFromCenter);

            ///
            /// MATH MODE ENGAGED!
            ///
            if (prevBotData.Piece.Section.SectionType != TrackType.StraightAway)
            {
              /// This previous piece is a curve. We need to break out the Law of Cosines x2 to determine the diagonal length of the path across the curve.
              /// First we must determine the length of the line which intersects the arc of this curve. It doesn't matter which side we take of the arc section of road. I'm going with the inner side.
              /// Law of Cosines: aa + bb - 2ab*COS(angle)

              /// Get inside lane.
              var insideLane = startLane;
              var offsetModifier = 1;
              if (prevBotData.Piece.Section.SectionType == TrackType.TurnRight)
              {
                offsetModifier = -1;// lane distance from center in right turns must be multiplied by -1.
                if (endLane.Index > startLane.Index)/// If the previous piece was a right turn and the endLane.Index is greater than the startLane.Index then the inside lane is the endLane. Else it is the startLane.
                  insideLane = endLane;
              }

              var isoscelesSides = endLane.DistanceFromCenter * offsetModifier + prevBotData.Piece.Radius;
              var trapezoidAngle = (180 - prevBotData.Piece.Angle) / 2;

              var intersectLineLength = MathUtil.LawOfCosines(isoscelesSides, isoscelesSides, prevBotData.Piece.Angle);
              previousPathTotalLength = MathUtil.LawOfCosines(intersectLineLength, horizontalDistanceTravelled, trapezoidAngle);
            }
            else
            {
              /// The previous pieve was a straightaway. Plain old pythag theorem will do the trick here.
              var previousPieceTotalLength = prevBotData.Piece.PieceLength(endLane.Index);
              previousPathTotalLength = MathUtil.PythagoreanTheorem(previousPieceTotalLength, horizontalDistanceTravelled);
            }
            ///
            /// MATH MODE DISENGAGED
            ///
          }

          currBotData.Velocity = (previousPathTotalLength - prevBotData.InPieceDistance) + currBotData.InPieceDistance;

          if (currBotData.Velocity < 0)
          {
            Logger.WriteLine(string.Format("{0} ({1}) has NEGATIVE VELOCITY! PPTL: {2}, PIPD: {3}, CIPD: {4}",
              currBotData.Name,
              currBotData.Color,
              previousPathTotalLength,
              prevBotData.InPieceDistance,
              currBotData.InPieceDistance));
            if (Color.Equals(currBotData.Color))
            {
              /// This is our car/bot
              /// Since we have a negative velocity we're going to calculate our velocity based on the previous data.
              _myCurrBotData.Velocity = _myPrevBotData.Velocity;
            }
          }
          else if (currBotData.Velocity >= 20)
          {
            Logger.WriteLine(string.Format("{0} ({1}) is out of whack!", currBotData.Name, currBotData.Color));
          }
        }
        else
        {
          /// In same piece as previous tick. Velocity equals change in distance.
          currBotData.Velocity = currBotData.InPieceDistance - prevBotData.InPieceDistance;
        }

        /// Determine bot's acceleration.
        currBotData.Acceleration = currBotData.Velocity - prevBotData.Velocity;
      }

      if (!_insideLaneReached)
      {
        _insideLaneReached = _raceData.Track.InsideLaneIndex == _myCurrBotData.EndLaneIndex;
      }
      else if (_raceData.Track.Lanes.Count > 2 &&
        _myCurrBotData.EndLaneIndex > 1)
      {
        _insideLaneReached = false;
      }

      /// if the acceleration coefficient has not yet been determined and we have acceleration data
      /// in our previous bot data then we can calculate the accleration coefficient.
      if (_accelCoef == 0.0 && _myPrevBotData.Acceleration > 0)
      {
        _accelCoef = _myCurrBotData.Acceleration / _myPrevBotData.Acceleration;
        _drag = MathUtil.Drag(_myPrevBotData.Velocity, _myCurrBotData.Velocity, 1.0);
        _mass = MathUtil.Mass(_myPrevBotData.Velocity, _myCurrBotData.Velocity, 1.0, _drag);
      }
    }

    private SendMessageBase DetermineAction()
    {
      SendMessageBase actionMsg = null;

      if (_myCurrBotData.Piece.Switch && _switchData.SwitchPending)
        _switchData.Reset();
      if (_myCurrBotData.Piece.NextPiece.Switch && !_switchData.SwitchPending && _gameTick > 5)
      {
        _switchData.QueueSwitch = EvaluateSwitch();
      }

      /// if turbo is available and turbo is not active and we have crossed into a new piece
      /// and the current section is a straightaway with length >= 300
      /// then queue up turbo
      if (_turboData.HasTurbo && !_turboData.TurboActive && !_turboData.TurboPending)
      {
        //bool comingOutOfTurnAndNextSectionIsStraight = _myCurrBotData.Piece.Section.NextSection.SectionType == TrackType.StraightAway && _myCurrBotData.SectionLengthRemaining < 15,
        //  onFinalStretch = _myCurrBotData.Lap == _raceData.RaceSession.Laps - 1 && _raceData.Track.TrackSections[_raceData.Track.TrackSections.Count - 1].Equals(_myCurrBotData.Piece.Section);

        var nextSectionLength = _myCurrBotData.Piece.Section.NextSection.TotalLengths[_myCurrBotData.StartLaneIndex];
        if (_myCurrBotData.Lap == _raceData.RaceSession.Laps - 1 && _raceData.Track.TrackSections[_raceData.Track.TrackSections.Count - 1].Equals(_myCurrBotData.Piece.Section) ||
            _myCurrBotData.Piece.Section.NextSection.SectionType == TrackType.StraightAway && _myCurrBotData.SectionLengthRemaining < 15 &&
            (nextSectionLength > 400 ||
            _myCurrBotData.Piece.Section.NextSection.NextSection.SectionType == TrackType.StraightAway && nextSectionLength + _myCurrBotData.Piece.Section.NextSection.NextSection.TotalLengths[_myCurrBotData.StartLaneIndex] >= 400))
        {
          _turboData.QueueTurbo = true;
        }
      }



      if (_turboData.QueueTurbo) // prioritize turbo
      {
        actionMsg = new Turbo("Cowabunga, Dudes!");
        _turboData.TurboPending = true;
        _turboData.QueueTurbo = false;
      }
      else if (_switchData.QueueSwitch)
      {
        actionMsg = new SwitchLane(_switchData.SwitchDirection);
        _switchData.SwitchPending = true;
        _switchData.QueueSwitch = false;
      }
      else
      {
        actionMsg = CalculateThrottle();
      }

      return actionMsg;
    }

    private bool EvaluateSwitch()
    {
      var shouldSwitch = false;

      var nextSection = _myCurrBotData.Piece.Section.NextSection;
      while (nextSection.SectionType == TrackType.StraightAway)
        nextSection = nextSection.NextSection;

      if (nextSection.SectionType == TrackType.TurnRight)
      {
        _switchData.SwitchDirection = "Right";
        shouldSwitch = true;
      }
      else if (nextSection.SectionType == TrackType.TurnLeft)
      {
        _switchData.SwitchDirection = "Left";
        shouldSwitch = true;
      }

      if (!_insideLaneReached && shouldSwitch)
      {
        if (_raceData.Track.TrackDirection > 0 && _switchData.SwitchDirection == "Left" ||
          _raceData.Track.TrackDirection < 0 && _switchData.SwitchDirection == "Right")
        {
          _switchData.Reset();
          shouldSwitch = false;
        }
      }

      return shouldSwitch;
    }

    double targetVelocity = 6.5; // target velocity should be dynamic and depend on the intensity of the current track section and change 
    private Throttle CalculateThrottle()
    {
      bool recoveringFromTurbo = _myCurrBotData.Velocity > 10; // top velocity for a throttle of 1.0 will be 10. If we exceed that then we are under the effects of turbo.
      double lowerAngleThreshold = 40.0, // angle threshold should change depending on the slip of the track and intensity of the current 
        upperAngleThreshold = 50.0, // angle threshold should change depending on the slip of the track and intensity of the current 
        targetVelocityUpperMargin = 0.05, // margins are used to allow the velocity to vary
        targetVelocityLowerMargin = 0.1,// and not cause drastic shifts in throttle
        increasingAngleThreshold = 5.0,

        turboSectionRemainingThreshold = 400,
        sectionLengthRemaining = _myCurrBotData.SectionLengthRemaining,
        sectionLengthRemainingThreshold = 0.0,
        currentVelocity = _myCurrBotData.Velocity,
        currAbsAngle = Math.Abs(_myCurrBotData.Angle),
        prevAbsAngle = Math.Abs(_myPrevBotData.Angle),
        angleDelta = currAbsAngle - prevAbsAngle,
        angleAbsDelta = Math.Abs(angleDelta);


      /// START TEST
      //if (_myCurrBotData.Velocity == 0)
      //  isAcc = true;
      //if (isAcc && _myCurrBotData.Velocity < 10)
      //if (_myCurrBotData.Velocity < 7.228)
      //{
      //  return new Throttle(_throttle = 1.0, _gameTick);
      //}
      //else
      //{
      //  return new Throttle(_throttle = 0.7228, _gameTick);
      //}
      //if (_gameTick < 50)
      //{
      //  return new Throttle(_throttle = 1.0, _gameTick);
      //}
      //if (_gameTick < 200)
      //{
      //  return new Throttle(_throttle = 0.0, _gameTick);
      //}
      //if (_gameTick < 75)
      //{
      //  return new Throttle(_throttle = 0.5, _gameTick);
      //}
      //if (_gameTick < 100)
      //{
      //  return new Throttle(_throttle = 0.25, _gameTick);
      //}
      //else
      //{
      //  isAcc = false;
      //  return new Throttle(_throttle = 0.0);
      //}
      //if (_gameTick ==  61)
      //  Logger.WriteLine("BREAK!");
      /// END TEST


      var section = _myCurrBotData.Piece.Section;

      TrackType currentSectionType = section.SectionType,
        nextSectionType = section.NextSection.SectionType;

      if (_turboData.TurboActive && (nextSectionType == TrackType.StraightAway && sectionLengthRemaining + section.NextSection.TotalLengths[_myCurrBotData.EndLaneIndex] > turboSectionRemainingThreshold) ||
        _myCurrBotData.Lap == _raceData.RaceSession.Laps - 1 && _raceData.Track.TrackSections[_raceData.Track.TrackSections.Count - 1].Equals(_myCurrBotData.Piece.Section))
      {
        Logger.WriteLine("Turbo or final stretch");
        return new Throttle(_throttle = 1.0); /// GUN IT! Turbo is active and we have enough track remaining or we're on the final stretch!
      }

      if (recoveringFromTurbo)// || _turboData.TurboActive && sectionLengthRemaining < turboSectionRemainingThreshold && currentVelocity > targetVelocity + targetVelocityUpperMargin)
      {
        Logger.WriteLine("Recovering from turbo.");
        return new Throttle(_throttle = 0.0);
      }

      var lengthMod = 0.58 + (1 - _accelCoef);
      if (currentSectionType == TrackType.StraightAway)
      {
        if (section.NextSection.SectionType != TrackType.StraightAway)
        {
          if (section.NextSection.CenterRadius <= 50)
          {
            lengthMod = 1;
          }
        }
        targetVelocity = _vfTurnMap.ContainsKey(section.NextSection.CenterRadius) ? _vfTurnMap[section.NextSection.CenterRadius] : 10;
      }
      else
      {
        targetVelocity = _vfTurnMap.ContainsKey(section.CenterRadius) ? _vfTurnMap[section.CenterRadius] : 10;
        if (section.CenterRadius <= 50)
        {
          lowerAngleThreshold = 30;
        }
      }
      if (_drag > 0.1)
        targetVelocity *= ((_drag - 0.1) / 4 + 0.1) * 10;
      else
        targetVelocity *= (0.1 - ((0.1 - _drag) / 2)) * 10;
      sectionLengthRemainingThreshold = section.TotalLengths[_myCurrBotData.EndLaneIndex] * lengthMod;

      //if ((currentSectionType == TrackType.StraightAway && (nextSectionType == TrackType.StraightAway || section.NextSection.CenterRadius >= 200 || sectionLengthRemaining > 200) || // On a straight AND (the next piece is a straight or we have sufficient track remaining before the turn)
      //  currentSectionType != TrackType.StraightAway && currAbsAngle < lowerAngleThreshold && // In a turn AND our angle is below threshold AND (velocity is below target minus lower margin OR we are coming out of a turn and the next section is a straight)
      //  (currentVelocity < targetVelocity - targetVelocityLowerMargin || nextSectionType == TrackType.StraightAway && sectionLengthRemaining < 75))) // _myCurrBotData.Piece.Section.Apexes.Sum() / (_myCurrBotData.Piece.Section.Apexes.Count * 2)
      //{
      //  Logger.WriteLine("Kicking it up a notch!");
      //  return new Throttle(_throttle = 1.0);
      //}

      if (_turboData.TurboActive)
        targetVelocity = targetVelocity / _turboData.TurboFactor;

      if (currAbsAngle < lowerAngleThreshold && (currentSectionType == TrackType.StraightAway && (nextSectionType == TrackType.StraightAway || section.NextSection.CenterRadius >= 200 && section.NextSection.TotalAngle <= 30 || sectionLengthRemaining > sectionLengthRemainingThreshold || currentVelocity < targetVelocity - targetVelocityLowerMargin) || // On a straight AND (the next piece is a straight or we have sufficient track remaining before the turn)
        currentSectionType != TrackType.StraightAway && // In a turn AND our angle is below threshold AND (velocity is below target minus lower margin OR we are coming out of a turn and the next section is a straight)
        (currentVelocity < targetVelocity - targetVelocityLowerMargin || nextSectionType == TrackType.StraightAway && sectionLengthRemaining < 75 && section.NextSection.TotalLengths[_myCurrBotData.EndLaneIndex] > 100))) // _myCurrBotData.Piece.Section.Apexes.Sum() / (_myCurrBotData.Piece.Section.Apexes.Count * 2)
      {
        Logger.WriteLine("Kicking it up a notch!");
        return new Throttle(_throttle = 1.0);
      }

      if (currentVelocity > targetVelocity + targetVelocityUpperMargin || (currAbsAngle > upperAngleThreshold || (currAbsAngle > lowerAngleThreshold && currAbsAngle > increasingAngleThreshold)))
      {
        Logger.WriteLine("Exceeding track thresholds.");
        return new Throttle(_throttle = 0.0);
      }

      if (currAbsAngle > lowerAngleThreshold)
      {
        Logger.WriteLine("Curbing throttle!");
        return new Throttle(_throttle = 1.0 - (currAbsAngle / 100));
      }

      Logger.WriteLine("Cruising.");
      //return new Throttle(_throttle = 1.0);
      return new Throttle(_throttle = targetVelocity / 10);
    }
    #endregion
  }
}