using System;
using System.IO;

namespace HWO2014_TheLambdaOperators.Utils
{
  public class Logger : IDisposable
  {
    private static Logger _logger = null;
    private readonly Stream _stream = null;
    private readonly StreamWriter _writer = null;

    private Logger(Stream writableStream)
    {
      _stream = writableStream;
      _writer = new StreamWriter(writableStream) { AutoFlush = true };
    }

    public static void Initialize(Stream writableStream)
    {
      if (_logger == null)
        _logger = new Logger(writableStream);
    }

    public static void WriteLine(string text)
    {
      _logger._writer.WriteLine(text);
      Console.WriteLine(text);
    }

    public static void WriteLine(string format, params object[] arg)
    {
      _logger._writer.WriteLine(format, arg);
      Console.WriteLine(format, arg);
    }

    public static void WriteToFileThenDispose(string fileName)
    {
      if (_logger == null)
        return;

      if (!string.IsNullOrWhiteSpace(fileName))
      {
        Directory.CreateDirectory(Path.GetDirectoryName(fileName));
        using (var fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write))
        {
          _logger._stream.Seek(0, SeekOrigin.Begin);
          _logger._stream.CopyTo(fs);
        }
      }
      _logger.Dispose();
      _logger = null;
    }

    public void Dispose()
    {
      if (_writer != null)
        _writer.Dispose();
      if (_stream != null)
        _stream.Dispose();
    }
  }
}