using System;

namespace HWO2014_TheLambdaOperators.Utils
{
  public static class MathUtil
  {
    /// <summary>
    ///  Calculates the arc length of an angle.
    /// </summary>
    /// <param name="angle">The angle in degrees.</param>
    /// <param name="radius">The radius of the circle.</param>
    /// <returns>The calculated arc length.</returns>
    public static double ArcLength(double angle, double radius)
    {
      return radius * Math.PI * Math.Abs(angle) / 180;
    }

    /// <summary>
    ///  Calculates the arc length of an angle with an offset.
    /// </summary>
    /// <param name="angle">The angle in degrees.</param>
    /// <param name="radius">The radius of the circle.</param>
    /// <param name="offest">An offset of the radius.</param>
    /// <returns>The calculated arc length.</returns>
    public static double ArcLength(double angle, double radius, double offset)
    {
      return (radius + offset) * Math.PI * Math.Abs(angle) / 180;
    }

    public static double LawOfCosines(double sideA, double sideB, double angle)
    {
      return Math.Sqrt((Math.Pow(sideA, 2) + Math.Pow(sideB, 2)) - (2 * sideA * sideB * Math.Cos(angle * Math.PI / 180)));
    }

    public static double PythagoreanTheorem(double sideA, double sideB)
    {
      return Math.Sqrt(Math.Pow(sideA, 2) + Math.Pow(sideB, 2));
    }

    public static double Drag(double v1, double v2, double throttle)
    {
      return (v1 - (v2 - v1)) / (Math.Pow(v1, 2) * throttle);
    }

    public static double Mass(double v1, double v2, double throttle, double drag)
    {
      return 1.0 / (Math.Log((v2 - (throttle / drag)) / (v1 - (throttle / drag))) / (-drag));
    }

    public static double TerminalVelocity(double throttle, double drag)
    {
      return throttle / drag;
    }

    public static double VelocityInTicks(int ticks, double v0, double throttle, double drag, double mass)
    {
      return (v0 - (throttle / drag)) * Math.Pow(Math.E, (-drag * ticks) / mass) + (throttle / drag);
    }

    public static int TicksToVelocity(double vf, double vi, double throttle, double drag, double mass)
    {
      return (int)Math.Round((Math.Log((vf - (throttle / drag)) / (vi - (throttle / drag))) * mass) / (-drag), MidpointRounding.AwayFromZero);
    }

    public static double DistanceInTicks(int ticks, double v0, double throttle, double drag, double mass)
    {
      return (mass / drag) * (v0 - (throttle / drag)) * (1.0 - Math.Pow(Math.E, (-drag * ticks) / mass)) + (throttle / drag) * ticks;
    }

    public static double Throttle(double vf, double vi, double ticks, double drag, double mass)
    {
      return drag * (vf * Math.Pow(Math.E, (drag * ticks) / mass) - vi) / (Math.Pow(Math.E, (drag * ticks) / mass) - 1.0);
    }
  }
}
