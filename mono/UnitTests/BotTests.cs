﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HWO2014_TheLambdaOperators.Utils;

namespace UnitTests
{
  [TestClass]
  public class BotTests
  {
    [TestMethod]
    public void MathUtil_LawOfCosines_Test()
    {
      var side = 90f;
      var angle = 45f;
      var oppositeSideLength = MathUtil.LawOfCosines(side, side, angle);
      Assert.AreEqual(68.88302, oppositeSideLength, 0.003);
    }

    [TestMethod]
    public void DragTest()
    {
      var dragActual = MathUtil.Drag(0.2, 0.396, 1.0);
      Assert.AreEqual(0.1, dragActual, 0.0000000001);
    }

    [TestMethod]
    public void MassTest()
    {
      var massActual = MathUtil.Mass(0.2, 0.396, 1.0, 0.1);
      Assert.AreEqual(4.94983, massActual, 0.0000017);
    }

    [TestMethod]
    public void TerminalVelocityTest()
    {
      var vf = MathUtil.TerminalVelocity(1.0, 0.1);
      Assert.AreEqual(10, vf);
    }

    [TestMethod]
    public void VelocityInTicksAccelerationTest()
    {
      var velocityActual = MathUtil.VelocityInTicks(50, 0, 1, 0.1, 4.9498316452508835);
      Assert.AreEqual(6.358303, velocityActual, 0.0000015);
    }

    [TestMethod]
    public void VelocityInTicksDeccelerationTest()
    {
      var velocityActual = MathUtil.VelocityInTicks(50, 6.3583044218435631, 0, 0.1, 4.9498316452508835);
      Assert.AreEqual(2.3155, velocityActual, 0.0000017);
    }

    [TestMethod]
    public void TicksToVelocityTest()
    {
      var ticksActual = MathUtil.TicksToVelocity(6.3583044218435631, 0, 1, 0.1, 4.9498316452508835);
      Assert.AreEqual(50, ticksActual);
    }

    [TestMethod]
    public void DistanceInTicksTest()
    {
      var distanceActual = MathUtil.DistanceInTicks(50, 0.0, 1.0, 0.1, 4.9498316452508835);
      Assert.AreEqual(188.4431432427, distanceActual);
    }

    [TestMethod]
    public void ThrottleDeccelerationTest()
    {
      var throttleActual = MathUtil.Throttle(2.3155016871992578, 6.3583044218435631, 50, 0.1, 4.9498316452508835);
      Assert.AreEqual(0.0, throttleActual);
    }

    [TestMethod]
    public void ThrottleAccelerationTest()
    {
      var throttleActual = MathUtil.Throttle(6.3583044218435631, 0.0, 50, 0.1, 4.9498316452508835);
      Assert.AreEqual(1.0, throttleActual, 0.00001);
    }

    [TestMethod]
    public void TicksToVelocityMassTest()
    {
      for (int i = 1800; i > 250; i--)
      {
        var vi = (double)i / 100;
        var vf = 2.5;
        var ticks = MathUtil.TicksToVelocity(vf, vi, 0, 0.1, 4.9498316452508835);
        Console.WriteLine("{0}\t{1}", vi, ticks);
      }
    }

    [TestMethod]
    public void TicksToVelocityMassTest2()
    {
      for (int i = 0; i < 800; i++)
      {
        var vi = (double)i / 100;
        var vf = 8;
        var ticks = MathUtil.TicksToVelocity(vf, vi, 1, 0.1, 4.9498316452508835);
        Console.WriteLine("{0}\t{1}", vi, ticks);
      }
    }

    [TestMethod]
    public void TicksToVelocityMassTest3()
    {
      var accelerate = true;
      int ia = 0;
      for (int i = 0; true; i++)
      {
        if (accelerate)
        {
          var vf = MathUtil.VelocityInTicks(i, 2, 1, 0.1, 4.9498316452508835);
          Console.WriteLine("{0}\t{1}", i, vf);
          if (vf > 8)
          {
            accelerate = false;
            ia = i + 1;
          }
        }
        else
        {
          var vf = MathUtil.VelocityInTicks(i - ia, 8, 0, 0.1, 4.9498316452508835);
          Console.WriteLine("{0}\t{1}", i, vf);
          if (vf < 2)
            break;
        }
      }
    }
  }
}
